<?php
    // sanitize inputs, unsets all variables that do not adhere to the standards
    $sane = TRUE;
    $IMPORT = TRUE;
    require("includes/input-sanitization.php");
    // set defaults for creating a new note, will be overridden by deletion request if applicable
    $targetId = $_POST["targetId"]??"";
    $inputTitle = $_POST["inputTitle"]??"";
    $inputType = $_POST["inputType"]??"notice";
    $inputDate = $_POST["inputDate"]??"";
    $inputDesc = $_POST["inputDescription"]??"";
    $inputLink = $_POST["inputLink"]??"";
    $inputLink = $inputLink == "#" ? "":$inputLink;
    $reqMode = $_POST["request"]??"create";
    $alertFlag = "hidden";
    if (isset($_POST["inputPassword"]) && $sane) {
        $IMPORT = TRUE;
        require_once("includes/handle_data.php");
        // auth pw
        if ($_POST["inputPassword"] == load_pw()) {
            // perform according handler
            if ($_POST["request"] == "create") {
                notes_create($inputTitle, $inputType, $inputDate, $inputDesc, $inputLink);
            } else if ($_POST["request"] == "delete") {
                notes_delete($targetId);
            } else if ($_POST["request"] == "edit") {
                notes_edit($targetId, $inputTitle, $inputType, $inputDate, $inputDesc, $inputLink);
            }
            // redirect to table
            header("location: table.php");
        }
        $insane_invalids["inputPassword"] = " is-invalid";
        // false pw, recover to form, maintain input values, mode etc
        $sane = FALSE;
    }
    if (!$sane) {
        $alertFlag = "";
    }
    if (isset($_GET["request"]) && $_GET["request"] == "delete") {
        // requested deletion, inputs are filled with note values from data
        $targetId = $_GET["targetId"];
        $IMPORT = TRUE;
        require_once("includes/handle_data.php");
        $targetNote = loadjson_notes()->{"notes"}[$targetId];
        $inputTitle = $targetNote->{"title"};
        $inputType = $targetNote->{"type"};
        $inputDate = $targetNote->{"date"};
        $inputDesc = $targetNote->{"desc"};
        $inputLink = $targetNote->{"link"};
        // set next request mode to delete
        $reqMode = "delete";
    }
    if (isset($_GET["request"]) && $_GET["request"] == "edit") {
        // requested edit, inputs are filled with note values from data
        $targetId = $_GET["targetId"];
        $IMPORT = TRUE;
        require_once("includes/handle_data.php");
        $targetNote = loadjson_notes()->{"notes"}[$targetId];
        $inputTitle = $targetNote->{"title"};
        $inputType = $targetNote->{"type"};
        $inputDate = $targetNote->{"date"};
        $inputDesc = $targetNote->{"desc"};
        $inputLink = $targetNote->{"link"};
        // set next request mode to delete
        $reqMode = "edit";
    }
    // put readonly on inputs if delete mode
    $iRO = ($reqMode == "delete");
    // color and text button from mode only
    $subBtnStyle = ($reqMode == "delete") ? "btn-danger" : "btn-success";
    $subBtnText = ($reqMode == "delete") ? "DELETE" : "SAVE";
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- meta tags and bootstrap css -->
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="res/css/bootstrap.min.css">
        <!-- custom head -->
        <title>SimpleTimeTable</title>
        <link rel="shortcut icon" href="favicon.png" />
    </head>
    <body style="overflow-y: scroll;">
        
        <div class="container-fluid form-row mt-3">

            <div class="col-lg-3"></div>

            <div class="col-lg-6">

                <h1 class="d-inline">Note Details</h1>
                <a href="table.php" class="btn btn-outline-danger float-right mt-2">Cancel</a>

                <div class="alert alert-warning mt-3" role="alert" <?php echo($alertFlag); ?>>
                    An error has occured while parsing your inputs.
                </div>

                <form action="note-details.php" method="post" class="mt-3">
                    <input type="text" name="request" value="<?php echo($reqMode); ?>" hidden readonly/>
                    <input type="text" name="targetId" value="<?php echo($targetId); ?>" hidden readonly/>
                    <div class="form-row">
                        <div class="form-group col-lg-5">
                            <label for="inputTitle">Title<b class="text-danger"> *</b></label>
                            <input type="text" class="form-control<?php echo($insane_invalids["inputTitle"]??""); ?>" id="inputTitle" name="inputTitle" placeholder="" maxlength="30" value="<?php echo($inputTitle); ?>" <?php echo($iRO?"readonly":""); ?> required>
                        </div>
                        <div class="form-group col-lg-4">
                            <label for="inputType">Importancy Type<b class="text-danger"> *</b></label>
                            <select id="inputType" name="inputType" class="form-control" <?php echo($iRO?"disabled":""); ?>>
                                <option <?php echo($inputType=="notice"?"selected":""); ?> value="notice">Notice (White)</option>
                                <option <?php echo($inputType=="info"?"selected":""); ?> value="info">Info (Cyan)</option>
                                <option <?php echo($inputType=="announcement"?"selected":""); ?> value="announcement">Announcement (Green)</option>
                                <option <?php echo($inputType=="warning"?"selected":""); ?> value="warning">Warning (Yellow)</option>
                                <option <?php echo($inputType=="critical"?"selected":""); ?> value="critical">Critical (Red)</option>
                            </select>
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="inputDate">Date<b class="text-danger"> *</b></label>
                            <input type="date" class="form-control<?php echo($insane_invalids["inputDate"]??""); ?>" id="inputDate" name="inputDate" min="<?php echo(date("Y-m-d")); ?>" value="<?php echo($inputDate); ?>" <?php echo($iRO?"readonly":""); ?> required>
                        </div>
                        <div class="w-100"></div>
                        <div class="form-group col-12">
                            <label for="inputDescription">Description<b class="text-danger"> *</b></label>
                            <textarea class="form-control<?php echo($insane_invalids["inputDescription"]??""); ?>" id="inputDescription" name="inputDescription" rows="3" maxlength="150" placeholder="Provide a short content for your note." style="resize: none;" <?php echo($iRO?"readonly":""); ?>><?php echo($inputDesc == "#" ? "" : $inputDesc); ?></textarea>
                        </div>
                        <div class="form-group col-12">
                            <label for="inputLink">Link<i class="text-warning"> (optional)</i></label>
                            <input type="text" class="form-control<?php echo($insane_invalids["inputLink"]??""); ?>" id="inputLink" name="inputLink" placeholder="---" value="<?php echo($inputLink); ?>" <?php echo($iRO?"readonly":""); ?>>
                        </div>
                        <div class="w-100"></div>
                        <div class="form-group col-lg-4">
                            <label for="inputPassword">Password<b class="text-danger"> *</b></label>
                            <input type="password" class="form-control<?php echo($insane_invalids["inputPassword"]??""); ?>" id="inputPassword" name="inputPassword" placeholder="***" maxlength="64" required>
                        </div>
                        <div class="col-lg-8 mt-2 mb-3"><br>
                            <button type="submit" class="btn btn-block <?php echo($subBtnStyle); ?>"><b><?php echo($subBtnText); ?></b></button>
                        </div>
                    </div>
                </form>

            </div>
            
            <div class="col-lg-3"></div>

        </div>

        <!-- bootstrap js and jquery slim -->
        <script src="res/js/jquery-3.4.1.slim.min.js"></script>
        <script src="res/js/bootstrap.bundle.min.js"></script>
    </body>
</html>