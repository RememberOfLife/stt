# SimpleTimeTable (STT)

This project serves the function of a *relatively* fast online timetable reference document. Best used with tables not changing in less than a few months.

## Features

* Dynamic loading of the configured timetable and topic list.
* All data is kept in a human readable json format.
* NGINX compatible by using import-guards.
* Note system provides easy and persistent communication for groups with a shared timetable. (Comes with colors!)
* Automatically (lazily) delete expired notes that are not at least relevant to the current day.
* Simple bootstrap design.

## How to Use

1. Clone the repo into a directory onto your web server (e.g. nginx or apache).
2. Immediately and before anything else, change the password in the `data/authn.php` file.
    * Note: you may want to hide the `.git` directory after cloning. See one of the following:
    * Deleting it does the trick but makes subsequent pulling hard..
    * Put an `.htaccess` in it containing `DENY FROM ALL`. (apache only)
    * Configure your nginx to not serve `.git` dirs: [see here.](https://gist.github.com/jaxbot/5748513) (last checked 28.11.2019; safe at time of check).
3. Setup your custom table and topics according to this guide.
    * To prevent your changes to the table and topics from being overwritten on a pull of a new version, configure STT in custom files rather than the delivered defaults.
4. Enjoy your STT.
5. (See throubleshooting for common mistakes.)

## Setting a Password

The password used by STT can be configured in the `data/authn.php` file. Simply make a copy of `data/default-authn.php` and name it `data/authn.php` then replace the default password in it with yours.

## Creating a Table

The main/fallback timetable used by STT can be configured in the `data/table.json` file.  
All other varying timetables may be configured in files: `data/table_ABC123.json` where the `ABC123` matches the regex: `^[a-zA-Z0-9]{3,9}$` as an ID. To use these custom varying tables supply a fitting get parameter to the URL used for the table: `table.php?tableID=ABC123`.

The file contains an array of times, which are used to determine the start and end times of the 6 timeslots and an array of days, one for each working day `MO-FR`. Timeslots can be configured as seen in the following example.

* Colors `0-6` can be used for easier differentiation of timeslots.
* Inside the `items` of an item in `days` the `ds` id is unique for that day. It represents the placement of that timeslot in the day according to `times`. The items of each day do not have to be sorted.

Reference colors as set in the code:

|Id|Color|
|---|---|
|0|Green|
|1|Blue|
|2|Turquoise (Cyan)|
|3|Red|
|4|Yellow|
|5|Gray|
|6|Ashwhite|

Even though json does not support comments, pretend that `/* ... */` is just repetition of previously shown examples.
```json
{
    "times": [
        {
            "timestr": "7:30-9:00",
            "ds": 1
        },

        /* fill in times 2 to 5 */

        {
            "timestr": "16:30-18:00",
            "ds": 6
        }
    ],
    "days": [
        {
            "name": "MO",
            "items": [
                {
                    "ds": 2,
                    "name": "Lecture XYZ",
                    "location": "[Building Room]",
                    "color": 2
                },

                /* ... */

                {
                    "ds": 6,
                    "name": "Meeting",
                    "location": "[Building Room]",
                    "color": 6
                }
            ]
        },

        /* same for TU to TH */

        {
            "name": "FR",
            "items": [
                /* ... */
            ]
        }
    ]
}
```

If you have to manage a timeslot that contains a weekly changing entry, i.e. this in week one and that in week two, swapping each week. Then you can use the following notation on that timeslot after the `ds` instead of the usual information.

```json
{
    "ds": 3,
    "variants": [
        {
            "week": 1,
            "name": "something in week one",
            "location": "[here]",
            "color": 2
        },
        {
            "week": 2,
            "name": "week two is filled here",
            "location": "[there]",
            "color": 3
        }
    ]
}
```

## Creating a Topic List

The topics used by STT can be configured in the `data/topics.json` file.

* Fill out as many topics as you need here, best used relating to the timeslots above.
* Colors `0-6` can be used for easier differentiation of topics. (Same as for the table.)
* If username and/or password are not used in a topic, simply keep the field as a blank string `""`.
* You can also create multiple links with different names, they will be spaced out a little for better readability.
* Remember: no trailing commas.

```json
{
    "topics": [
        {
            "name": "Lecture",
            "color": 0,
            "links": [
                {
                    "desc": "link-name",
                    "href": "https:// ..."
                }

                /* ... */

            ],
            "username": "un to the website if applicable",
            "password": "somepw"
        }

        /* ... */

    ]
}
```

## Manually changing Notes

It is recommended not to manually edit notes, it is however very much possible - easily so.  
The notes used by STT can be configured in the `data/notes.json` file.

* Not every note needs a link, if left blank this field will not be shown in the notifications bar.
* Date has to be in the future or current day, otherwise the note will get instantly deleted upon loading the table.
* Type may only be one of: `[notice, info, announcement, warning, critical]`.

```json
{
    "notes": [
        {
            "title": "foo",
            "type": "notice",
            "date": "YYYY-MM-DD",
            "desc": "bar",
            "link": ""
        }

        /* ... */

    ]
}
```

## Troubleshooting

* **Anything else**
    * Create an issue or send a pull request.
