
function copyToClipboard(that){
    // uses tmp input to select because topic code is not a selectable input field
    var tmp = document.createElement('input');
    document.body.appendChild(tmp);
    tmp.value = that.textContent;
    tmp.select();
    document.execCommand('copy', false);
    tmp.remove();
    // add fading background spike to signify copy action success
    var target = $(that);
    target.addClass("copyToClip-anim");
    setTimeout(function() {
        target.removeClass("copyToClip-anim");
    }, 500);
}
