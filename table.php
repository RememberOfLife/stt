<?php
    $IMPORT = FALSE;
    // some clean button somewhere or test on each load to remove outdated notes (date is over by a whole day)
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- meta tags and bootstrap css -->
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="res/css/bootstrap.min.css">
        <!-- custom table stylings -->
        <link rel="stylesheet" href="res/css/table.css">
        <!-- custom head -->
        <title>SimpleTimeTable</title>
        <link rel="shortcut icon" href="favicon.png"/>
    </head>
    <body style="overflow-y: scroll;">
        
        <div class="container-fluid mt-3">

        <div class="row">

            <div class="col-lg-9">

                <?php
                    $time_lastSA = strtotime("last saturday");
                    $time_nextFR = strtotime("next friday");
                    $timestr_lastSA = date("Y-m-d", $time_lastSA);
                    $timestr_nextFR = date("Y-m-d", $time_nextFR);

                    // look for possible custom table specifications, if none load main/fallback table
                    $tableID = $_GET["tableID"] ?? NULL;
                ?>
                <h1 class="mb-0 d-inline">SimpleTimeTable&emsp;<?php echo($timestr_lastSA . '&ensp;&ndash;&ensp;' . $timestr_nextFR); ?></h1>
                <code class="mt-2 mb-3 float-right h3 text-secondary">T#<?php echo($tableID == NULL ? "-" : $tableID); ?></code>

                <!-- timetable -->
                <table class="table table-bordered">
                    <thead>
                        <th scope="col" style="width: 12%;">Zeit</th>
                        <th scope="col" style="width: 4%;">DS</th>
                        <th scope="col" style="width: 16.8%;">Monday</th>
                        <th scope="col" style="width: 16.8%;">Tuesday</th>
                        <th scope="col" style="width: 16.8%;">Wednesday</th>
                        <th scope="col" style="width: 16.8%;">Thursday</th>
                        <th scope="col" style="width: 16.8%;">Friday</th>
                    </thead>
                    <tbody>

                        <?php

                            $colorpalette = [
                                0 => "success",
                                1 => "primary",
                                2 => "info",
                                3 => "danger",
                                4 => "warning",
                                5 => "secondary",
                                6 => "light",
                            ];

                            
                            $IMPORT = TRUE;
                            require_once("includes/handle_data.php");

                            $table = loadjson_table($tableID);
                            $table_times = $table->{"times"};
                            $table_days = $table->{"days"};
                            $table_full = [];

                            // +1 to the date week num if only 2 days since last sat have passed
                            $weekedif = (time() - $time_lastSA) / (24 * 60 * 60);
                            $weekedif = ($weekedif > 2) ? 0 : 1;
                            $weekNum = ((date("W") + $weekedif) % 2 == 0) ? 1 : 0;

                            for ($i = 0; $i < 5; $i++) {
                                $table_day = $table_days[$i];
                                $table_day_slots = [];
                                foreach ($table_day->{"items"} as $table_day_items) {
                                    $table_day_slots[$table_day_items->{"ds"}] = $table_day_items;
                                }
                                for ($j = 1; $j < 7; $j++) {
                                    $table_fullfill_targetClass = "";
                                    $table_fullfill_targetString = "-";
                                    $table_fullfill_targetNotice = "";

                                    if (isset($table_day_slots[$j])) {
                                        $tff_sane = TRUE;
                                        if (isset($table_day_slots[$j]->{"variants"})) {
                                            // assign the slot the actual values since it only contains variants
                                            if (isset($table_day_slots[$j]->{"variants"}[$weekNum])) {
                                                $table_day_slots[$j]->{"name"} = $table_day_slots[$j]->{"variants"}[$weekNum]->{"name"};
                                                $table_day_slots[$j]->{"location"} = $table_day_slots[$j]->{"variants"}[$weekNum]->{"location"};
                                                $table_day_slots[$j]->{"color"} = $table_day_slots[$j]->{"variants"}[$weekNum]->{"color"};
                                                if (isset($table_day_slots[$j]->{"variants"}[($weekNum+1)%2])) {
                                                    $variantNoticeText = $table_day_slots[$j]->{"variants"}[($weekNum+1)%2]->{"name"} . '<br>' . $table_day_slots[$j]->{"variants"}[($weekNum+1)%2]->{"location"};
                                                    $variantNoticeClass = $colorpalette[$table_day_slots[$j]->{"variants"}[($weekNum+1)%2]->{"color"}];
                                                    $table_fullfill_targetNotice = '<span class="float-right"><img src="res/img/repeat.svg" alt="edit" height="20" data-toggle="tooltip" data-placement="top" data-html="true" title="' . $variantNoticeText . '" data-template=\'<div class="tooltip" role="tooltip"><div class="arrow"></div><div class="tooltip-inner border border-dark text-left bg-' . $variantNoticeClass . '"></div></div>\'></span>';
                                                }
                                            } else {
                                                $tff_sane = FALSE;
                                            }
                                        }
                                        if ($tff_sane) {
                                            $table_fullfill_targetClass = ' class="table-' . $colorpalette[$table_day_slots[$j]->{"color"}] . '"';
                                            $table_fullfill_targetString = $table_day_slots[$j]->{"name"} . '<br>' . $table_day_slots[$j]->{"location"};
                                        }
                                    }

                                    $table_full[$j][$i] = [
                                        0 => $table_fullfill_targetClass,
                                        1 => $table_fullfill_targetString,
                                        2 => $table_fullfill_targetNotice,
                                    ];
                                }
                            }

                            for ($i = 0; $i < 6; $i++) {
                                $table_row_time = $table_times[$i]->{"timestr"};
                                $table_row_ds = $table_times[$i]->{"ds"};
                                $table_row_full = $table_full[$i+1];
                                $IMPORT = TRUE;
                                require("includes/table-row.php");
                            }
                            
                        ?>

                    </tbody>
                </table>

                <!-- link collection and passwords -->
                <table class="table table-sm table-bordered">
                    <thead>
                        <tr>
                            <th scope="col" style="width: 15%;">Topic</th>
                            <th scope="col" style="width: 35%;">Links</th>
                            <th scope="col" style="width: 25%;">Username</th>
                            <th scope="col" style="width: 25%;">Password</th>
                        </tr>
                    </thead>
                    <tbody>

                        <?php
                            
                            $IMPORT = TRUE;
                            require_once("includes/handle_data.php");
                            $topics = loadjson_topics();

                            foreach ($topics->{"topics"} as $topic_row) {
                                $topic_row_cc = ' class="table-' . $colorpalette[$topic_row->{"color"}] . '"';
                                $topic_row_name = $topic_row->{"name"};
                                $topic_row_links = $topic_row->{"links"};
                                $topic_row_username = $topic_row->{"username"}?:"-";
                                $topic_row_password = $topic_row->{"password"}?:"-";
                                $IMPORT = TRUE;
                                require("includes/topic-row.php");
                            }

                        ?>
                        
                    </tbody>
                </table>

                
            </div>
            
            <div class="col-lg-3">
                <!-- news board -->
                <div class="card mb-3">
                    <?php

                        $IMPORT = TRUE;
                        require_once("includes/handle_data.php");
                        notes_softpurge();
                        $notes = loadjson_notes();

                        $note_card_typetable = [
                            "notice" => ["", ""],
                            "info" => [" border-info", " text-info"],
                            "announcement" => [" border-success", " text-success"],
                            "warning" => [" border-warning", " text-warning"],
                            "critical" => [" border-danger", " text-danger"],
                        ];

                    ?>
                    <div class="card-header"><h5 class="card-title mb-0">NOTIFICATIONS &ndash; <?php echo(count($notes->{"notes"})); ?></h5></div>
                    <div class="card-body p-2">
                        <a href="note-details.php" class="btn btn-outline-success btn-block">Create Note</a>
                    </div>
                </div>
                <?php

                    foreach ($notes->{"notes"} as $id => $note) {
                        $note_card_date = $note->{"date"};
                        $note_card_id = $id;
                        $note_card_title = htmlspecialchars($note->{"title"});
                        $note_card_type = $note->{"type"};
                        $note_card_desc = htmlspecialchars($note->{"desc"});
                        $note_card_link = $note->{"link"};
                        $IMPORT = TRUE;
                        require("includes/note-card.php");
                    }

                ?>&nbsp;
            </div>

        </div>

        </div>

        <nav class="navbar navbar-light bg-light">
            DISCLAIMER: ALL LINKS ON THIS PAGE ARE PROVIDED "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED.<br>
            THEY HAVE BEEN CHECKED AT TIME OF POSTING BUT PROVIDE NO FURTHER SECURITIES.
            <div class="ml-auto">
                    <a href="https://gitlab.com/RememberOfLife/stt">Open-Source</a>
            </div>
        </nav>

        <!-- bootstrap js and jquery slim -->
        <script src="res/js/jquery-3.4.1.slim.min.js"></script>
        <script src="res/js/bootstrap.bundle.min.js"></script>
        <!-- table functionality -->
        <script src="res/js/table.js"></script>
        <script>
            // initialize all tooltips
            $(function () {
                $('[data-toggle="tooltip"]').tooltip()
            })
        </script>
    </body>
</html>