<?php
    if (!isset($IMPORT) || !$IMPORT) {
        die("424 - Failed Dependency: The request failed because it depended on another request that has not preceeded it.");
    } $IMPORT = FALSE;
    // these can just get unset if they do not pass requirements
    $insan_soft_fail_post = [
        "targetId" => "fcb_Id",
        "inputTitle" => "fcb_StrTitle",
        "inputType" => "fcb_Type",
        "inputDate" => "fcb_Date",
        "inputDescription" => "fcb_StrDesc",
        "inputLink" => "fcb_Link",
        "request" => "fcb_Req",
        "inputPassword" => "fcb_PW",
    ];
    $insane_invalids = [
        "targetId" => NULL,
        "inputTitle" => NULL,
        "inputType" => NULL,
        "inputDate" => NULL,
        "inputDescription" => NULL,
        "inputLink" => NULL,
        "request" => NULL,
        "inputPassword" => NULL,
    ];
    // callbacks return null if not passed so that note-details can assume them failed/unset
    function fcb_Id($value) {
        if (!is_numeric($value) || $value < 0) {
            return -1;
        }
        return $value;
    }
    function fcb_StrTitle($value) {
        return $value;
    }
    function fcb_StrDesc($value) {
        return $value == NULL ? "#" : $value;
    }
    function fcb_Date($value) {
        $d = DateTime::createFromFormat("Y-m-d", $value);
        if (!($d && $d->format("Y-m-d") === $value)) {
            return NULL;
        }
        return $value;
    }
    function fcb_Type($value) {
        if (!in_array($value, array("notice", "info", "announcement", "warning", "critical"))) {
            return NULL;
        }
        return $value;
    }
    function fcb_Link($value) {
        if ($value != NULL && filter_var($value, FILTER_VALIDATE_URL) == FALSE) {
            return NULL;
        }
        if ($value == NULL) {
            return "#";
        }
        return $value;
    }
    function fcb_Req($value) {
        if (!in_array($value, array("create", "delete", "edit"))) {
            return NULL;
        }
        return $value;
    }
    function fcb_PW($value) {
        return $value;
    }
    foreach ($_POST as $key => $parg) {
        if (!isset($insan_soft_fail_post[$key])) {
            unset($_POST[$key]);
        } else {
            // perform san-check
            $_POST[$key] = filter_var($parg, FILTER_CALLBACK, array("options" => $insan_soft_fail_post[$key]));
            if ($_POST[$key] == NULL) {
                $insane_invalids[$key] = " is-invalid";
                $sane = FALSE;
            }
        }
    }

    // if these dont match, unset and header to default
    if (
        isset($_GET["request"]) && (
            !isset($_GET["targetId"])
            || !is_numeric($_GET["targetId"])
            || $_GET["targetId"] < 0
            || !in_array($_GET["request"], array("delete", "edit"))
        )
    ) {
        unset($_GET["request"]);
        unset($_GET["targetId"]);
        header("location: note-details.php");
    }
?>