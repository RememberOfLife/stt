<?php
    if (!isset($IMPORT) || !$IMPORT) {
        die("424 - Failed Dependency: The request failed because it depended on another request that has not preceeded it.");
    } $IMPORT = FALSE;
    if (!isset($note_card_typetable)) die("500 - Internal Server Error");
?>
<div class="card mb-3<?php echo($note_card_typetable[$note_card_type][0]); ?>">
                    <div class="card-header">
                        <h5 class="card-title mb-0 d-inline<?php echo($note_card_typetable[$note_card_type][1]); ?>"><?php echo ($note_card_title); ?></h5>
                        <form action="note-details.php" method="get" class="d-inline float-right">
                            <input type="text" name="targetId" value="<?php echo ($note_card_id); ?>" hidden readonly/>
                            <button type="submit" name="request" value="edit" class="p-0 border-0 note-card-btn mr-2">
                                <img src="res/img/edit.svg" alt="edit" height="16">
                            </button>
                            <button type="submit" name="request" value="delete" class="close"><span>&times;</span></button>
                        </form>
                    </div>
                    <div class="card-body px-3 pb-2 pt-3">
                        <h6 class="card-subtitle mb-1 text-muted">Relevant: <mark><?php echo ($note_card_date); ?></mark>
                            <?php
                                if ($note_card_link != NULL) {
                                    echo ('<a href="' . $note_card_link . '" class="card-link float-right">LINK</a>&nbsp');
                                }
                            ?>
                        </h6>
                        <p class="card-text"><?php echo ($note_card_desc == "#" ? "" : $note_card_desc); ?></p>
                    </div>
                </div>