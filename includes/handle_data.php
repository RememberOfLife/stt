<?php
    if (!isset($IMPORT) || !$IMPORT) {
        die("424 - Failed Dependency: The request failed because it depended on another request that has not preceeded it.");
    } $IMPORT = FALSE;

    function notes_create($title, $type, $date, $desc, $link) {
        $notes = loadjson_notes();
        $note = new \stdClass();
        $note->{"title"} = $title;
        $note->{"type"} = $type;
        $note->{"date"} = $date;
        $note->{"desc"} = $desc;
        $note->{"link"} = $link;
        $i = 0;
        while (isset($notes->{"notes"}[$i])) { $i++; }
        $notes->{"notes"}[$i] = $note;
        usort($notes->{"notes"}, function($a, $b) {
            return new DateTime($a->{"date"}) <=> new DateTime($b->{"date"});
        });
        file_put_contents("data/notes.json", json_encode($notes, JSON_PRETTY_PRINT));
    }

    function notes_delete($id) {
        $notes = loadjson_notes();
        unset($notes->{"notes"}[$id]);
        $notes->{"notes"} = array_values($notes->{"notes"});
        file_put_contents("data/notes.json", json_encode($notes, JSON_PRETTY_PRINT));
    }

    function notes_edit($id, $title, $type, $date, $desc, $link) {
        notes_delete($id);
        notes_create($title, $type, $date, $desc, $link);
    }

    function notes_softpurge() {
        $notes = loadjson_notes();
        $notes_expiries = 0;
        foreach ($notes->{"notes"} as $id => $note) {
            $note_card_date = $note->{"date"};
            if (strtotime($note_card_date) < time()-60*60*24) {
                notes_delete($id - $notes_expiries);
                $notes_expiries++;
            }
        }
    }

    function loadjson_table($tableID) {
        // sanitize tableID
        $tableID = preg_match("/^[a-zA-Z0-9]{3,9}$/", $tableID) ? "_".$tableID : "";
        if (file_exists("data/table" . $tableID . ".json")) {
            return json_decode(file_get_contents("data/table" . $tableID . ".json"));
        }
        return json_decode(file_get_contents("data/default-table.json"));
    }

    function loadjson_topics() {
        if (file_exists("data/topics.json")) {
            return json_decode(file_get_contents("data/topics.json"));
        }
        return json_decode(file_get_contents("data/default-topics.json"));
    }

    function loadjson_notes() {
        if (!file_exists("data/notes.json")) {
            $notes = new \stdClass;
            $notes->{"notes"} = array();
            file_put_contents("data/notes.json", json_encode($notes, JSON_PRETTY_PRINT));
        }
        return json_decode(file_get_contents("data/notes.json"));
    }

    function load_pw() {
        if (!file_exists("data/authn.php")) {
            die();
        }
        $IMPORT = TRUE;
        require("data/authn.php");
        return $TARGET_PASSWORD;
    }

?>